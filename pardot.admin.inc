<?php
/**
 * @file
 * Admin form
 */

/**
 * Settings form for general pardot information.
 */
function pardot_admin_form() {
  global $language;
  $lang = isset($language->language) ? $language->language : 'en';
  $form = array();

  $form['pardot_a_id' . $lang] = array(
    '#title' => t('Pardot account id'),
    '#type' => 'textfield',
    '#description' => t('The value show in the pardot demo script for piAId. eg. if the script has piAId = "1001"; this field should be 1001'),
    '#default_value' => variable_get('pardot_a_id' . $lang, ''),
  );
  $form['pardot_c_id' . $lang] = array(
    '#title' => t('Default Pardot campaign id'),
    '#type' => 'textfield',
    '#description' => t('The value show in the pardot demo script for piCId. eg. if the script has piCId = "1001"; this field should be 1001'),
    '#default_value' => variable_get('pardot_c_id' . $lang, ''),
  );
  $form['pardot']['pardot_validate_url'] = array (
    '#title' => t('Validate Pardot url'),
    '#type' => 'checkbox',
    '#description' => t('Validate Pardot URL, turn off to not use validation'),
    '#default_value' => variable_get('pardot_validate_url', 1),
  );

  return system_settings_form($form);
}

function pardot_webform_components_form($form, $form_state, $webform_node) {
  $form = array();

  $form['#tree'] = TRUE;
  $form['#node'] = $webform_node;
  $nid = $webform_node->nid;
  $record = pardot_webform_load($nid);

  $form['#record'] = $record;

  if (!isset($record->is_active)) {
   $record = new stdClass();
   $record->is_active = "";
   $record->url = "";
  }
  $form['details'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
  );
  $form['details']['is_active'] = array(
    '#title' => 'Is active',
    '#type' => 'checkbox',
    '#default_value' => $record->is_active,
  );
  $form['details']['url'] = array(
    '#title' => 'Post url',
    '#type' => 'textfield',
    '#default_value' => $record->url,
    '#description' => t('Visit your form handler page in pardot and select "View form handler code", copy the url out of the example code and then paste it here. <br />!example', array(
      '!example' => theme('image', array('path' => drupal_get_path('module', 'pardot') . '/form-handler-url.jpg')),
    )),
  );

  $form['components'] = array(
    '#tree' => TRUE,
  );
  foreach ($webform_node->webform['components'] as $k => $component) {
    $form['components'][$k] = array(
      '#component' => $component,
      'key' => array(
        '#title' => 'Field name',
        '#type'  => 'textfield',
        '#size' => 25,
        '#default_value' => isset($record->data[$component['form_key']]['key']) ? $record->data[$component['form_key']]['key'] : '',
      ),
    );
  }
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('pardot_webform_components_form_delete_submit'),
  );

  return $form;
}

function pardot_webform_components_form_submit($form, $form_state) {
  $node = $form['#node'];

  $record = $form['#record'];
  if ($record) {
    $update = array('nid');
  }
  else {
    $record = new stdClass();
    $update = array();
  }

  $data = array();
  foreach (element_children($form['components']) as $k) {
    $component = $form['components'][$k]['#component'];
    $data[$component['form_key']]['key'] = $form_state['values']['components'][$k]['key'];
  }
  $record->nid = $node->nid;
  $record->url = $form_state['values']['details']['url'];
  $record->is_active = (bool) $form_state['values']['details']['is_active'];
  $record->data = $data;
  drupal_write_record('pardot_webform', $record, $update);
}

/**
 * Implements hook_form().
 */
function theme_pardot_webform_components_form($form) {
  $form = $form['form'];
  $rows = array();
  $output = '';

  $header = array(t('Name'), t('Type'), t('Pardot key'));

  foreach (element_children($form['components']) as $k) {
    $row = array();
    // Name
    $row[] = $form['#node']->webform['components'][$k]['name'];
    // Type
    $row[] = $form['#node']->webform['components'][$k]['type'];
    // Pardot key
    unset($form['components'][$k]['key']['#title']);
    $row[] = drupal_render($form['components'][$k]['key']);
    $rows[] = $row;
  }

  $output .= drupal_render($form['details']);
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Implements hook_form_validate().
 *
 * Basic validation.
 */
function pardot_webform_components_form_validate($form, $form_state) {
  // Check for Pardot validate URL variable, if not set, set it to TRUE (1).
  $pardot_validate_url = variable_get('pardot_validate_url', 1);

  // Validation of URL should be done
  if ($pardot_validate_url == 1) {
    if (function_exists('curl_getinfo')) {
      // Just check if the URL is active. No validation done if it is not.
      if($form_state['values']['details']['is_active'] == 1) {

        // If so, check if it's a valid url
        $check_url = curl_init($form_state['values']['details']['url']);
        // Use curl_setopt to get the response.
        curl_setopt($check_url, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($check_url, CURLOPT_HEADER, TRUE);

        // Fetch the url.
        $response = curl_exec($check_url);
        if(!$response) {
          drupal_set_message(t('Could not connect to host, no validation done for post URL'),'status');
        }
        else {
          // Get the pardot cookie if it exists.
          preg_match('/^Set-Cookie:\s*([^;]*)/mi', $response, $cookie_result);
          parse_str($cookie_result[1], $cookie);

          // If the Pardot cookie exists.
          if (isset($cookie['pardot'])) {
            // Check for HTTP code, we need a 200.
            $http_code = curl_getinfo($check_url, CURLINFO_HTTP_CODE);
            if($http_code != 200) {
              // Set form error.
              form_set_error('url', t('The Post url is not valid.'));
            }
          }
          // We are missing the Pardot cookie.
          else {
            form_set_error('url', t('This is not a pardot Post url.'));
          }
          // Always close an opened url.
          curl_close($check_url);
        }
      }
      else {
        // Print out message.
        drupal_set_message(t('No validation done for none active Post URL'), 'status');
      }
    }
  }
}

/**
 * Deletes the pardot post url from the database for the node.
 */
function pardot_webform_components_form_delete_submit($form, &$form_state) {
  $node = $form['#node'];
  db_delete('pardot_webform')
    ->condition('nid', $node->nid)
    ->execute();

  drupal_set_message(t('Pardot settings for this webform deleted.'), 'status');
}
