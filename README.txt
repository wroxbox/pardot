This is my upgrade project to update  pardot 6.x [1] module to drupal 7.

The other sandbox -project [2] has only minimal pardot script implementation. This tries to be the exact same with functionality as 6.x

Pardot is Marketing automation and analytics software.

This integration adds support to track campaigns and submit webform data to pardot with form handlers -method [3] described in faq.

[1] = http://drupal.org/project/pardot
[2] = https://drupal.org/sandbox/ekristen/1751376
[3] = http://www.pardot.com/help/faqs/forms/form-handlers
